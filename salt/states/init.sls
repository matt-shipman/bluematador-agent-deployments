{%- set config = salt['pillar.get']('bluematador-agent', {}) %}
{%- set kernel = salt['grains.get']('kernel') %}

{%- if salt['grains.get']('os_family') == "RedHat" %}
bluematador.repo:
  pkgrepo.managed:
    - hummanname: Blue Matador, Inc.
    - baseurl: http://yum.bluematador.com/
    - gpgcheck: 1
    - enabled: true
    - gpgkey: http://yum.bluematador.com/OPSBUNKER_RPM_KEY.public
{%- endif %}

{%- if salt['grains.get']('os_family') == "Debian" %}
bluematador.repo:
  pkgrepo.managed:
    - hummanname: Blue Matador, Inc.
    - name: deb https://apt.bluematador.com stable main
    - keyserver: hkp://keyserver.ubuntu.com:80
    - keyid: BBED0D5A
{%- endif %}

install-agent:
  pkg.installed:
    - name: bluematador-agent

/etc/bluematador-agent/config.ini:
  file.managed:
    - source: salt://config.ini.jinja
    - template: jinja
    - makedirs: True
    - context:
      account_id: {{ config["account_id"] }}
      api_key: {{ config["api_key"] }}

bluematador-agent:
  service.running:
    - enable: True
    - restart: True
    - watch:
      - file: /etc/bluematador-agent/config.ini